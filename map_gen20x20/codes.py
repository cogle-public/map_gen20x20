from enum import Enum, IntEnum

class Codes(IntEnum):
    TREES = 1     # Height 1, trees - light green
    GRASS = 2     # Height 0, grass - light green
    ROAD = 10     # Height 0, road - green grey
    AIRTRAFFIC_TOWER = 13   # Height 2, tower - light green (firewatch tower and air traffic tower)
    WATER = 15    # Height 0, water - blue
    FIREWATCH_TOWER = 17    # Height 2, tower - light green (firewatch tower and air traffic tower)
    HILL = 24     # Height 1, hills - light grey
    HIGH_HILL = 25          # Height 2, hills - grey
    FOOTHILL = 26 # Height 3, foothill - dark grey
    MOUNTAIN = 31 # Height 4, mountains - black
            
    # Specials
    PACKAGE = 100
    BROKEN_PACKAGE = 101
    HIKER = 102
    DROP_POINT = 103            # Intended drop point
    DROP_POINT_BULLSEYE = 104   # Directly on top of intended drop
    EXPLOSION = 105             # Craft crash or other explosion
    DROP_POINT_ARROW = 106      # Arrow for indicating on path where drop happened
    START = 107
    STOP = 108


class Colors(Enum):
    WHITE = "white"
    BLACK = "black"
    ORANGE = "orange"
    RED = "red"
    PINK = "pink"
    PURPLE = "purple"
    BLUE = "blue"
    TEAL = "teal"
    
