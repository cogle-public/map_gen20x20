import ast
import os
from io import BytesIO
import base64
import math
import copy
import json
from PIL import Image, ImageDraw

from .codes import Codes, Colors
from .mappings import BG_COLOR_DICT, IMG_PATH_DICT, COLOR_DICT

class MapGenerator():
    def __init__(self, map=[], width=400, height=400, side=20, mode=0):
        self.CANVAS_WIDTH  = width
        self.CANVAS_HEIGHT = height
        self.SIDE_LENGTH = side

        self.world = map
        self.specials = {}
        self.mode = mode

        self.side = side

        self.DEFAULT_ICON_SIZE = 40  # type: int #Icons are scaled at 40x40 so they can be seen while editing
        self.SPECIAL_PACKAGE = 100
        self.SPECIAL_DROP_POINT = 103
        self.SPECIAL_PACKAGE_ON_DROP_POINT = 104

        self.top_icon_positions = []

        self.penalty_config = {}

        # Load in config
        this_dir = os.path.dirname(os.path.realpath(__file__))
        config_path = os.path.join(this_dir, 'penalty_config.json')
        with open(config_path, 'r') as config_f:
            self.penalty_config = json.load(config_f)

    def hex_to_RGB(self, hex):
        ''' "#FFFFFF" -> [255,255,255] '''
        # Pass 16 to the integer function for change of base
        return [int(hex[i:i+2], 16) for i in range(1,6,2)]


    def linear_gradient(self, start_hex, finish_hex="#FFFFFF", n=307):
        ''' returns a gradient list of (n) colors between
            two hex colors. start_hex and finish_hex
            should be the full six-digit color string,
            inlcuding the number sign ("#FFFFFF") '''
        # Starting and ending colors in RGB form
        s = self.hex_to_RGB(start_hex)
        f = self.hex_to_RGB(finish_hex)
        # Initilize a list of the output colors with the starting color
        RGB_list = [s]

        # Calcuate a color at each evenly spaced value of t from 1 to n
        for t in range(1, n):
            # Interpolate RGB vector for color at the current value of t
            curr_vector = [int(s[j] + (float(t)/(n-1))*(f[j]-s[j])) for j in range(3)]
            # Add it to our list of output colors
            RGB_list.append(curr_vector)

        return RGB_list


    def _scale_coordinates(self, generator, image_width, image_height, side_length=0):
        scaled_width = int(image_width / side_length)
        scaled_height = int(image_height / side_length)

        for coords in generator(scaled_width, scaled_height):
            yield [(x * side_length, y * side_length) for (x, y) in coords]


    def generate_unit_squares(self, image_width, image_height):
        """Generate coordinates for a tiling of unit squares."""
        # Iterate over the required rows and cells.  The for loops (x, y)
        # give the coordinates of the top left-hand corner of each square:
        #
        #      (x, y) +-----+ (x + 1, y)
        #             |     |
        #             |     |
        #             |     |
        #  (x, y + 1) +-----+ (x + 1, y + 1)
        #
        for x in range(image_width):
            for y in range(image_height):
                yield [(x, y), (x + 1, y), (x + 1, y + 1), (x, y + 1)]


    def generate_squares(self, *args, **kwargs):
        """Generate coordinates for a tiling of squares."""
        return self._scale_coordinates(self.generate_unit_squares, *args, **kwargs)

    def is_odd(self, num):
        if (num % 2) == 0:
            return False
        else:
            return True


    def scale_path_points(self, path):
        def default_index_value(arr, index, default_value=0):
            if len(arr) > index:
                return arr[index]
            else:
                return default_value

        #x_unit = self.CANVAS_WIDTH // self.SIDE_LENGTH
        x_unit = self.SIDE_LENGTH
        x_offset = self.SIDE_LENGTH // 2

        #y_unit = self.CANVAS_HEIGHT // self.SIDE_LENGTH
        y_unit = self.SIDE_LENGTH
        y_offset = self.SIDE_LENGTH // 2

        new_path = []
        #print (self.CANVAS_WIDTH, self.CANVAS_HEIGHT, self.SIDE_LENGTH)
        #print (x_unit,y_unit,x_offset,y_offset)

        for point in path:
            x = default_index_value(point,0)
            y = default_index_value(point,1)
            z = default_index_value(point,2)
            h = default_index_value(point,3)

            new_x = x * x_unit + x_offset
            new_y = y * y_unit + y_offset

            #print(x,y,new_x,new_y)

            new_path.append((new_x, new_y, z, h))

        return new_path

    def draw_path_line(self, draw, segment, seg_alt, color):
        if seg_alt == 0:
            height = 1
        elif seg_alt == 1:
            height = 2
        elif seg_alt == 2:
            height = 6
        elif seg_alt == 3:
            height = 10
        else:
            height = 0
        draw.line(xy=segment, fill=color, width=height)

    def draw_box(self, draw, x, y, w, h, color, value):

        square_im = Image.new('RGBA', size=(self.CANVAS_WIDTH, self.CANVAS_HEIGHT))

        opacity = 26   # 10% opacity

        if value >= 1:
            opacity = 140   # 55%
        elif value >= 0.75:
            opacity = 102   # 40% 
        elif value >= 0.5:
            opacity = 51    # 20%
        elif value >= 0.25:
            opacity = 26    # 10%

        fill_color = (color[0], color[1], color[2], opacity)
        outline_color = COLOR_DICT[Colors.WHITE]
        # print(outline_color)

        ImageDraw.Draw(square_im).rectangle([x, y, x+w, y+h], fill=fill_color, outline=outline_color, width=1)
        
        return Image.alpha_composite(draw, square_im)

    def draw_arrow(self, draw, segment, alt, color, heading):
        points = [segment]
        sz2 = math.floor((self.CANVAS_WIDTH / self.SIDE_LENGTH) / 2)
        if alt == 1:
            sz2 = sz2 - 4
        elif alt == 2:
            sz2 = sz2 - 3
        elif alt == 0:
            sz2 = sz2 - 0

        #sza = sz2 + 4
        sza = sz2 + 1 + alt

        if heading == 0:
            points.append((segment[0]-sz2,  segment[1]-sz2))
            points.append((segment[0]+sz2,  segment[1]+sz2))
        elif heading == 1:
            points.append((segment[0]-sz2,  segment[1]+sz2))
            points.append((segment[0]+sz2,  segment[1]+sz2))
        elif heading == 2:
            points.append((segment[0]-sza,  segment[1]))
            points.append((segment[0],      segment[1]+sza))
        elif heading == 3:
            points.append((segment[0]-sz2,  segment[1]-sz2))
            points.append((segment[0]-sz2,  segment[1]+sz2))
        elif heading == 4:
            points.append((segment[0]-sza,  segment[1]))
            points.append((segment[0],      segment[1]-sza))
        elif heading == 5:
            points.append((segment[0]-sz2,  segment[1]-sz2))
            points.append((segment[0]+sz2,  segment[1]-sz2))
        elif heading == 6:
            points.append((segment[0],      segment[1]-sza))
            points.append((segment[0]+sza,  segment[1]))
        elif heading == 7:
            points.append((segment[0]+sz2,  segment[1]-sz2))
            points.append((segment[0]+sz2,  segment[1]+sz2))
        elif heading == 8:
            points.append((segment[0]+sza,   segment[1]))
            points.append((segment[0],       segment[1]+sza))

        draw.polygon(points, fill=color, outline=(128,128,128,200))
        #print("Arrow at " + str(points) + ", heading: " + str(heading))

    def draw_path(self, a_path, im, start='#FF0000', end='#FFFFFF', draw_arrow=False):

        if a_path is not None and len(a_path) > 1:
            new_path = self.scale_path_points(a_path)

            #start = '#660000'
            #end = '#7F1AE5'

            #start = '#630000'
            #end = '#FFC6A5'

            #start = '#FF0000'
            #end = '#FFFFFF'

            #start = '#390031'
            #end = '#DEBDDE'
            color_cycle = self.linear_gradient(end, start, n=len(new_path*2))

            # ImageDraw.Draw(im).line(xy=new_path, fill=color_cycle, width=2)

            seq = 1
            path_length = len(new_path)

            #print("Wooooooo hooooooo!!!!")

            draw = ImageDraw.Draw(im, 'RGBA')

            for start in new_path:
                segment1 = []
                segment2 = []
                end = new_path[seq]
                mid = (start[0] + math.floor((end[0] - start[0]) / 2),  start[1] + math.floor((end[1] - start[1]) / 2))

                segment1.append((start[0],start[1]))
                segment1.append((mid[0],mid[1]))

                segment2.append((mid[0],mid[1]))
                segment2.append((end[0], end[1]))

                seq2x = seq*2
                color1 = (color_cycle[seq2x][0], color_cycle[seq2x][1], color_cycle[seq2x][2], 164)
                color2 = (color_cycle[seq2x+1][0], color_cycle[seq2x+1][1], color_cycle[seq2x+1][2], 164)

                self.draw_path_line(draw, segment1, start[2], color1)

                seq += 1
                if seq == path_length:
                    if draw_arrow:
                        self.draw_arrow(draw, (end[0], end[1]), end[2], color2, end[3])
                    break
                self.draw_path_line(draw, segment2, end[2], color2)

            del draw

    def get_special(self, specialInx, altImgInx = None):
        # Todo: *MAYBE* Check for special Special case of Drop Package and Drop Point being at the same location?
        dir_path = os.path.dirname(os.path.realpath(__file__))
        img_path = ''
        if altImgInx:
            img_path = IMG_PATH_DICT[altImgInx]
        else:
            img_path = IMG_PATH_DICT[specialInx]

        if img_path is None:
            return None

        try:
            file_path = os.path.join(dir_path, img_path)
        except TypeError as e:
            print("ERROR IN THE MAP GENERATOR FOR {}!".format(specialInx))
            raise TypeError
        img = Image.open(file_path)
        return img

    def draw_tiling(self,
                    coord_generator,
                    filename,
                    world,
                    path=[],
                    paths=[],
                    special=None,
                    output_file=False,
                    return_raw_bytes=False,
                    return_bytes=False,
                    start_color=None,
                    end_color=None,
                    minor_path=None,
                    overlayVals=None,
                    overlayMonochrome=True,
                    schema="4"):
        """
        Given a coordinate generator and a filename, render those coordinates
        in a new image and save them to the file.

        Color each tile as appropriate with the world type for that tile.
        """
        #print("DRAWING MAP " + str(filename))

        specials = copy.copy(self.specials)

        if type(special) is dict:
            specials.update(special)
            entries = list(special.keys())
            if len(entries) > 0:
                with_time = len(entries[0]) == 3
                if with_time:
                    # print("Found time bounded specials: {}".format(special))
                    pruned = {}
                    for pos, val in specials.items():
                        pruned_pos = (pos[0], pos[1])
                        pruned_val = val
                        if type(val) != list:
                            pruned_val = [val]
                        if pruned_pos in pruned:
                            if type(pruned[pruned_pos]) == list:
                                pruned[pruned_pos].extend(pruned_val)
                            else:
                                pruned[pruned_pos] = pruned_val
                        else:
                            pruned[pruned_pos] = pruned_val
                    specials = pruned
        # print(specials)

        path_start_colors = ['#FF000040', '#80008040', '#FFA50040', '#A500FF40']
        path_end_colors = ['#FFFFFF40', '#FFFFFF40', '#FFFFFF40', '#FFFFFF40']

        im = Image.new('RGBA', size=(self.CANVAS_WIDTH, self.CANVAS_HEIGHT))
        path_im = Image.new('RGBA', size=(self.CANVAS_WIDTH, self.CANVAS_HEIGHT))
        icon_im = Image.new('RGBA', size=(self.CANVAS_WIDTH, self.CANVAS_HEIGHT))
        overlay_im = Image.new('RGBA', size=(self.CANVAS_WIDTH, self.CANVAS_HEIGHT))

        self.top_icon_positions = []
        column = 0
        row = 0

        for shape in coord_generator(self.CANVAS_WIDTH, self.CANVAS_HEIGHT, self.SIDE_LENGTH):
            this_color_index = world[row][column]
            fill_color = COLOR_DICT[this_color_index]

            not_a_special = True

            if (row, column) in specials:
                #print("----- Found a special: " + str(special))
                #fill_color = special[(row, column)]
                # not_a_special = False
                for sp in specials[(row, column)]:
                    # print("Using icon for {} at {}".format(sp, (column, row)))
                    self.top_icon_positions.append((sp, shape))

            if self.mode == 1 and IMG_PATH_DICT[this_color_index] is not None:
                #print(this_color_index, IMG_PATH_DICT[this_color_index])
                dir_path = os.path.dirname(os.path.realpath(__file__))
                file_path = os.path.join(dir_path, IMG_PATH_DICT[this_color_index])
                img = Image.open(file_path)
                if self.side != self.DEFAULT_ICON_SIZE:
                    size = (self.side, self.side)
                    img.thumbnail(size, Image.ANTIALIAS)
                ImageDraw.Draw(im).polygon(shape, fill=BG_COLOR_DICT[this_color_index])
                if not_a_special:
                    ImageDraw.Draw(im).bitmap(xy=shape[0], bitmap=img, fill=fill_color)
            else:
                ImageDraw.Draw(im).polygon(shape, fill=fill_color)

            #print("Column:" + str(column) + ", Row: " + str(row) + " -- " + str(world[row][column]))

            row += 1
            if row >= 20:
                column += 1
                row = 0

        pathix = 0
        if len(paths) > 0:
            for path in paths:
                self.draw_path(path, path_im, start=path_start_colors[pathix], end=path_end_colors[pathix], draw_arrow=True)
                pathix += 1
                # Simple default to guard against there being more color
                if pathix>3:
                    pathix = 0
        else:
            if start_color is not None and end_color is not None:
                self.draw_path(path, path_im, start=start_color, end=end_color, draw_arrow=True)
            else:
                self.draw_path(path, path_im, start=path_start_colors[pathix], end=path_end_colors[pathix], draw_arrow=True)
        im = Image.alpha_composite(im, path_im)

        # Draw icons on top of everything else
        for item, shape in self.top_icon_positions:
            # ONLY 2 ICONS supported
            if self.mode == 1 and not item == 102:
                # Using icons
                #print("Add image " + str(item) + " at " + str(shape[0]) )
                img = self.get_special(item)
                if img is None:
                    continue

                if self.side != self.DEFAULT_ICON_SIZE:
                    size = (self.side, self.side)
                    img.thumbnail(size, Image.ANTIALIAS)
                im.paste(img, shape[0], img)

        for item, shape in self.top_icon_positions:
            # ONLY 2 ICONS supported
            if self.mode == 1 and item == 102:
                # Using icons
                #print("Add image " + str(item) + " at " + str(shape[0]) )
                img = self.get_special(item)
                if self.side != self.DEFAULT_ICON_SIZE:
                    size = (self.side, self.side)
                    img.thumbnail(size, Image.ANTIALIAS)
                im.paste(img, shape[0], img)

        #im = Image.alpha_composite(im, icon_im)

        if minor_path is not None:
            # print("Drawing minor path: {}".format(minor_path))
            self.draw_path(minor_path, im, start='#FDE55B', end='#FDE55B', draw_arrow=False)

        # Draw overlay
        if overlayVals is not None and type(overlayVals) is dict:
            schema_id = schema
            if type(schema_id) is int:
                schema_id = str(schema)
            if schema_id not in self.penalty_config:
                for sid, sval in self.penalty_config.items():
                    if schema_id in sval['variants']:
                        schema_id = sid
                        break
                else:
                    raise ValueError("Invalid schema used for overlay map generation: {}".format(schema_id))

            for category, contents in overlayVals.items():
                group_id = ''
                color_id = self.penalty_config[schema_id]['monochromeColor'].upper()
                color = COLOR_DICT[Colors[color_id]]
                for factor_id, factor_obj in self.penalty_config[schema_id]['factors'].items():
                    if category in factor_obj['subVals']:
                        group_id = factor_id
                        if not overlayMonochrome:
                            color_id = self.penalty_config[schema_id]['factors'][factor_id]['color'].upper()
                            color = COLOR_DICT[Colors[color_id]]
                    
                for step in contents:
                    if len(step[2]) > 0:    # Sometimes list of spaces will be empty
                        avg = self.penalty_config[schema_id]['factors'][group_id]['subVals'][category]['penaltyAvg']

                        if type(step[2][0]) is list:
                            for spaces in step[2]:
                                score = spaces[0]
                                x = spaces[1][0]
                                y = spaces[1][1]

                                # Draw square
                                value = score/avg
                                overlay_im = self.draw_box(overlay_im, x*self.SIDE_LENGTH, y*self.SIDE_LENGTH, self.SIDE_LENGTH, self.SIDE_LENGTH, color, value)
                        else:
                            score = step[2][0]
                            x = step[2][1][0]
                            y = step[2][1][1]

                            # Draw square
                            value = score/avg
                            overlay_im = self.draw_box(overlay_im, x*self.SIDE_LENGTH, y*self.SIDE_LENGTH, self.SIDE_LENGTH, self.SIDE_LENGTH, color, value)

            im = Image.alpha_composite(im, overlay_im)       

        if output_file:
            im.save(filename)

        if return_raw_bytes:
            output = BytesIO()
            im.save(output, format='PNG')
            im_data = output.getvalue()
            return output
        elif return_bytes:
            output = BytesIO()
            im.save(output, format='PNG')
            im_data = output.getvalue()
            # msg = b"<plain_txt_msg:img>"
            # msg = msg + base64.b64encode(im_data)
            msg = base64.b64encode(im_data)
            return msg
        else:
            return None

    """
    filename (string): path for new image file
    special (dict): special icons (hiker, drop point) to draw in addition to map, trace, and terrain icons.
                    Key/Value pair is coordinates (x,y) / list of icon codes augments colored tiles with icons.
    path (list): list of size 4 tuples (x, y, z, heading) that represents a path to draw. 
    paths (list): 2D list - list of lists of size 4 tuples that represent a set of paths to draw
    start_color (string): Hexstring with hash code for the color of the start of the path
    end_color (string): Hexstring with hash code for the color of the end of the path
    """
    def create_file(self, 
                    filename='images/squares.png', 
                    path=[], 
                    paths=[], 
                    special=None, 
                    start_color='#FFFFFF40', 
                    end_color='#FF000040', 
                    minor_path=None,
                    overlayVals=None, 
                    overlayMonochrome=True,
                    schema="4"):

        self.draw_tiling(self.generate_squares, filename, world=self.world, 
                         path=path, paths=paths, output_file=True, special=special, 
                         start_color=start_color, end_color=end_color,
                         minor_path=minor_path, overlayVals=overlayVals, overlayMonochrome=overlayMonochrome, schema=schema)

    def get_raw_bytes(self, 
                      path=[],
                      paths=[],
                      special=None, 
                      start_color='#FFFFFF40', 
                      end_color='#FF000040', 
                      minor_path=None,
                      overlayVals=None,
                      overlayMonochrome=True,
                      schema="4"):

        return self.draw_tiling(self.generate_squares, filename='no.png', world=self.world, 
                                path=path, paths=paths, output_file=False, return_raw_bytes=True, return_bytes=False, 
                                special=special, start_color=start_color, end_color=end_color, minor_path=minor_path,
                                overlayVals=overlayVals, overlayMonochrome=overlayMonochrome, schema=schema)
    
    def get_bytes(self,
                  path=[], 
                  paths=[], 
                  special=None, 
                  start_color='#FFFFFF40', 
                  end_color='#FF000040', 
                  minor_path=None,
                  overlayVals=None,
                  overlayMonochrome=True,
                  schema="4"):

        return self.draw_tiling(self.generate_squares, filename='no.png', world=self.world, 
                                path=path, paths=paths, output_file=False, return_bytes=True,
                                special=special, start_color=start_color, end_color=end_color, 
                                minor_path=minor_path, overlayVals=overlayVals, overlayMonochrome=overlayMonochrome)

    def create_file_and_get_bytes(self, 
                                  filename='images/squares.png', 
                                  path=[], 
                                  paths=[], 
                                  special=None,
                                  start_color='#FFFFFF40', 
                                  end_color='#FF000040',
                                  minor_path=None,
                                  overlayVals=None,
                                  overlayMonochrome=True,
                                  schema="4"):

        return self.draw_tiling(self.generate_squares, filename, world=self.world, 
                                path=path, paths=paths, output_file=True, return_bytes=True, 
                                minor_path=minor_path, special=special, overlayVals=overlayVals, overlayMonochrome=overlayMonochrome, schema=schema)
