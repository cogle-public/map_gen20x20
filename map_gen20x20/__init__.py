from .map_generator import MapGenerator
from .alt_generator import AltGenerator
from .codes import Codes, Colors