import ast
import os
from io import BytesIO
import base64
import math
import copy
from PIL import Image, ImageDraw, ImageFont

from .mappings import COLOR_DICT, IMG_PATH_DICT
from .codes import Codes

class AltGenerator():
    def __init__(self, map=[], width=400, height=400, side=20, mode=0):
        self.CANVAS_WIDTH  = width
        self.CANVAS_HEIGHT = height
        self.SIDE_LENGTH = side

        self.world = map
        self.specials = {}
        self.mode = mode

        self.side = side

        self.DEFAULT_ICON_SIZE = 40  # type: int #Icons are scaled at 40x40 so they can be seen while editing

        self.PATH_COLOR = (255, 0, 128, 255)
        self.top_icon_positions = []

        self.typeToAltDict = {
            2: 0,
            24: 1,
            1: 1,
            25: 2,
            26: 3,
            31: 4,
            5:  4,
            15: 0,
            13: 2,
            17: 2,
            10: 0,
            # Specials
            100: 0,
            101: 0,
            102: 0,
            103: 0,
            104: 0
        }

    def hex_to_RGB(self, hex):
        ''' "#FFFFFF" -> [255,255,255] '''
        # Pass 16 to the integer function for change of base
        return [int(hex[i:i+2], 16) for i in range(1,6,2)]


    def linear_gradient(self, start_hex, finish_hex, n=307):
        ''' returns a gradient list of (n) colors between
            two hex colors. start_hex and finish_hex
            should be the full six-digit color string,
            inlcuding the number sign ("#FFFFFF") '''
        # Starting and ending colors in RGB form
        s = self.hex_to_RGB(start_hex)
        f = self.hex_to_RGB(finish_hex)
        # Initilize a list of the output colors with the starting color
        RGB_list = [s]

        # Calcuate a color at each evenly spaced value of t from 1 to n
        for t in range(1, n):
            # Interpolate RGB vector for color at the current value of t
            curr_vector = [int(s[j] + (float(t)/(n-1))*(f[j]-s[j])) for j in range(3)]
            # Add it to our list of output colors
            RGB_list.append(curr_vector)

        return RGB_list


    def _scale_coordinates(self, generator, image_width, image_height, side_length=0):
        scaled_width = int(image_width / side_length)
        scaled_height = int(image_height / side_length)

        for coords in generator(scaled_width, scaled_height):
            yield [(x * side_length, y * side_length) for (x, y) in coords]


    def generate_unit_squares(self, image_width, image_height):
        """Generate coordinates for a tiling of unit squares."""
        # Iterate over the required rows and cells.  The for loops (x, y)
        # give the coordinates of the top left-hand corner of each square:
        #
        #      (x, y) +-----+ (x + 1, y)
        #             |     |
        #             |     |
        #             |     |
        #  (x, y + 1) +-----+ (x + 1, y + 1)
        #
        for x in range(image_width):
            for y in range(image_height):
                yield [(x, y), (x + 1, y), (x + 1, y + 1), (x, y + 1)]


    def generate_squares(self, *args, **kwargs):
        """Generate coordinates for a tiling of squares."""
        return self._scale_coordinates(self.generate_unit_squares, *args, **kwargs)

    def is_odd(self, num):
        if (num % 2) == 0:
            return False
        else:
            return True


    def scale_path_points(self, path):
        def default_index_value(arr, index, default_value=0):
            if len(arr) > index:
                return arr[index]
            else:
                return default_value

        #x_unit = self.CANVAS_WIDTH // self.SIDE_LENGTH
        x_unit = self.SIDE_LENGTH
        x_offset = self.SIDE_LENGTH // 2

        #y_unit = self.CANVAS_HEIGHT // self.SIDE_LENGTH
        y_unit = self.SIDE_LENGTH
        y_offset = self.SIDE_LENGTH // 2

        new_path = []
        #print (self.CANVAS_WIDTH, self.CANVAS_HEIGHT, self.SIDE_LENGTH)
        #print (x_unit,y_unit,x_offset,y_offset)

        for point in path:
            x = default_index_value(point,0)
            y = default_index_value(point,1)
            z = default_index_value(point,2)
            h = default_index_value(point,3)

            new_x = x * x_unit + x_offset
            new_y = y * y_unit + y_offset

            #print(x,y,new_x,new_y)

            new_path.append((new_x, new_y, z, h))

        return new_path


    def draw_arrow(self, draw, path_background_im, segment, alt, color, heading):
        points = [segment]
        sz2 = math.floor((self.CANVAS_WIDTH / self.SIDE_LENGTH) / 2)

        #sza = sz2 + 4
        sza = sz2 + 1 + alt

        if heading == 0:
            points.append((segment[0]-sz2,  segment[1]-sz2))
            points.append((segment[0]+sz2,  segment[1]+sz2))
        elif heading == 1:
            points.append((segment[0]-sz2,  segment[1]+sz2))
            points.append((segment[0]+sz2,  segment[1]+sz2))
        elif heading == 2:
            points.append((segment[0]-sza,  segment[1]))
            points.append((segment[0],      segment[1]+sza))
        elif heading == 3:
            points.append((segment[0]-sz2,  segment[1]-sz2))
            points.append((segment[0]-sz2,  segment[1]+sz2))
        elif heading == 4:
            points.append((segment[0]-sza,  segment[1]))
            points.append((segment[0],      segment[1]-sza))
        elif heading == 5:
            points.append((segment[0]-sz2,  segment[1]-sz2))
            points.append((segment[0]+sz2,  segment[1]-sz2))
        elif heading == 6:
            points.append((segment[0],      segment[1]-sza))
            points.append((segment[0]+sza,  segment[1]))
        elif heading == 7:
            points.append((segment[0]+sz2,  segment[1]-sz2))
            points.append((segment[0]+sz2,  segment[1]+sz2))
        elif heading == 8:
            points.append((segment[0]+sza,   segment[1]))
            points.append((segment[0],       segment[1]+sza))

        ImageDraw.Draw(path_background_im).polygon(points, fill=COLOR_DICT[2])
        draw.polygon(points, fill=color, outline=(128,128,128,200))

    def get_special(self, specialInx, altImgInx=None):
        # Todo: *MAYBE* Check for special Special case of Drop Package and Drop Point being at the same location?
        dir_path = os.path.dirname(os.path.realpath(__file__))
        img_path = ''
        if altImgInx is not None:
            img_path = IMG_PATH_DICT[altImgInx]
        else:
            img_path = IMG_PATH_DICT[specialInx]
        try:
            file_path = os.path.join(dir_path, img_path)
        except TypeError as e:
            print("ERROR IN THE MAP GENERATOR FOR {}!".format(specialInx))
            raise TypeError
        img = Image.open(file_path)
        return img

    def draw_tiling(self,
                    coord_generator,
                    filename,
                    world,
                    path=[],
                    max_path_len=0,
                    paths=[],
                    special=None,
                    output_file=False,
                    return_bytes=False,
                    return_raw_bytes=False,
                    start_color='#FF000040',
                    end_color='#FFFFFF40',
                    minor_path=None):
        """
        Given a coordinate generator and a filename, render those coordinates
        in a new image and save them to the file.

        Color each tile as appropriate with the world type for that tile.
        """
        # print("**** DRAWING Alt " + str(filename))

        pathLen = max(1, len(path), max_path_len)

        htItem = int((self.CANVAS_HEIGHT / self.SIDE_LENGTH))
        htSize = int(htItem * 5)
        wItem = int(self.CANVAS_WIDTH / self.SIDE_LENGTH)
        wSize = wItem * pathLen
        xOffset = 20
        textYOffset = int(htItem/4)
        titleYOffset = 20

        path_im = Image.new('RGBA', size=(wSize + xOffset, htSize + titleYOffset))
        path_background_im = Image.new('RGBA', size=(wSize + xOffset, htSize + titleYOffset))

        inPath = {}

        #fnt = ImageFont.truetype('Pillow/Tests/fonts/FreeMono.ttf', 40)
        #fnt = ImageFont.truetype('Pillow/Tests/fonts/FreeMono.ttf', 40)

        ImageDraw.Draw(path_im).text((0, textYOffset), text='Alt', fill=(0,0,0,255))
        ImageDraw.Draw(path_im).text((int(wSize/2), textYOffset), text='Time', fill=(0,0,0,255))

        for a in range(0,5):
            ImageDraw.Draw(path_im).text((5, (4-a) * htItem + textYOffset + titleYOffset), text=str(a), fill=(0,0,0,255))


        rtAltsAt = {}
        encountered = set() # Icons in path that have already been encountered

        if len(path) > 0:

            color_cycle = self.linear_gradient(end_color, start_color,  n=len(path))

            seq = 0
            path_length = len(path)
            pPos = 0

            for timestep in range(path_length):

                p = path[timestep]

                x = pPos + xOffset
                y = (4-p[2]) * htItem + titleYOffset
                x2 = x + wItem
                y2 = y + htItem
                rect = ((x, y), (x2, y2))

                inPath[p] = x
                rtAltsAt[p] = y

                #print("Path element " + str(seq) + ', len ' + str(path_length) + ', ' + (str(p[0]) +'_'+ str(p[1])) + ', p: ' + str(p) + ', ' + str(p[2]))

                color1 = (color_cycle[seq][0], color_cycle[seq][1], color_cycle[seq][2], 144)
                seq += 1
                if seq == path_length:
                    self.draw_arrow(ImageDraw.Draw(path_im), path_background_im, (x+int(xOffset/2), y+int(htItem/2)), 3, color1, 3)
                else:
                    ImageDraw.Draw(path_background_im).rectangle(rect, fill=COLOR_DICT[2])
                    ImageDraw.Draw(path_im).rectangle(rect, fill=color1)

                if p[2] > 0:
                    rect = ((x, 4*htItem + titleYOffset), (x2, 5*htItem + titleYOffset))
                    ImageDraw.Draw(path_im).rectangle(rect, fill=COLOR_DICT[2])
                pPos = pPos + wItem

            path_im = Image.alpha_composite(path_background_im, path_im)

            self.top_icon_positions = []

            for timestep in range(path_length):
                pos = path[timestep]
                path_x, path_y, path_z, path_h = pos

                row = path_y
                column = path_x

                image_x = inPath[pos]

                this_color_index = world[row][column]
                fill_color = COLOR_DICT[this_color_index]
                alt = self.typeToAltDict[this_color_index]
                rect = ((image_x, 4*htItem + titleYOffset), (x + wItem, 4*htItem + titleYOffset + htItem))
                img_pos = (image_x, (4-alt)*htItem + titleYOffset)

                # print("*** [{}] found a terrain type: {}".format(timestep, this_color_index) + " item in path " + str(column) + ',' + str(row) + ', alt: ' + str(alt))

                # if (this_color_index == 26):
                #     print("<<<*** mt at: " + str(column) + ', ' + str(row) + "***>>>")

                dir_path = os.path.dirname(os.path.realpath(__file__))

                for special_moment, icons in special.items():
                    try:
                        spy, spx, spt = special_moment
                    except ValueError:
                        raise ValueError("Altitude Generator: Invalid special icon position {}".format(special_moment))

                    is_time = special_moment == (row, column, timestep)
                    is_time_default = (spx == column and spy == row and spt == 0)
                    # print("{} at {} | {}: {}".format(icons, special_moment, timestep, is_time))

                    if (is_time or is_time_default) and special_moment not in encountered:
                        for sp in icons:
                            # print("----- Found a special: " + str(sp) + " at: " + str(pos))
                            self.top_icon_positions.append((sp, img_pos))
                            if sp == Codes.DROP_POINT:
                                lpos = rtAltsAt[pos]
                                file_path = os.path.join(dir_path, IMG_PATH_DICT[Codes.DROP_POINT_ARROW])
                                img = Image.open(file_path)
                                ImageDraw.Draw(path_im).bitmap(xy=(img_pos[0], lpos), bitmap=img, fill=(0,0,0,255))

                        encountered.add(special_moment)
                        break

                if self.mode == 1 and IMG_PATH_DICT[this_color_index] is not None:
                    # print("Type: {} | path: {}".format(this_color_index, IMG_PATH_DICT[this_color_index]))
                    file_path = os.path.join(dir_path, IMG_PATH_DICT[this_color_index])
                    img = Image.open(file_path)
                    if self.side != self.DEFAULT_ICON_SIZE:
                        size = (self.side, self.side)
                        img.thumbnail(size, Image.ANTIALIAS)

                    for a in range(0,alt+1):
                        stRow = (4-a)*htItem + titleYOffset
                        rect = ((image_x, stRow), (image_x + wItem - 1, stRow + htItem - 1))
                        ImageDraw.Draw(path_im).rectangle(rect, fill=(0, 0, 0, 255))
                        ImageDraw.Draw(path_im).bitmap(xy=(image_x, stRow), bitmap=img, fill=fill_color)
                else:
                    ImageDraw.Draw(path_im).rectangle(rect, fill=fill_color)

            for a in range(0,alt+1):
                stRow = (4-a)*htItem + titleYOffset
                rect = ((image_x, stRow), (image_x + wItem, stRow + htItem ))
                #ImageDraw.Draw(path_im).rectangle(rect, fill=fill_color)

        # Draw icons on top of everything else
        for item, pos in self.top_icon_positions:
            # ONLY 2 ICONS supported
            if self.mode == 1 and not item == 102:
                # Using icons
                #print("Add image " + str(item) + " at " + str(pos) )
                img = self.get_special(item)
                if self.side != self.DEFAULT_ICON_SIZE:
                    size = (self.side, self.side)
                    img.thumbnail(size, Image.ANTIALIAS)
                path_im.paste(img, pos, img)

        for item, pos in self.top_icon_positions:
            # ONLY 2 ICONS supported
            if self.mode == 1 and item == 102:
                # Using icons
                #print("Add image " + str(item) + " at " + str(pos) )
                img = self.get_special(item)
                if self.side != self.DEFAULT_ICON_SIZE:
                    size = (self.side, self.side)
                    img.thumbnail(size, Image.ANTIALIAS)
                path_im.paste(img, pos, img)

        if output_file:
            path_im.save(filename)

        if return_raw_bytes:
            output = BytesIO()
            path_im.save(output, format='PNG')
            im_data = output.getvalue()
            return output
        elif return_bytes:
            output = BytesIO()
            path_im.save(output, format='PNG')
            im_data = output.getvalue()
            # msg = b"<plain_txt_msg:img>"
            # msg = msg + base64.b64encode(im_data)
            msg = base64.b64encode(im_data)
            return msg
        else:
            return None

    """
    filename (string): path for new image file
    special (dict): special icons (hiker, drop point) to draw in addition to map, trace, and terrain icons.
                    Key/Value pair is coordinates (x,y) / list of icon codes augments colored tiles with icons.
    path (list): list of size 4 tuples (x, y, z, heading) that represents a path to draw. 
    paths (list): 2D list - list of lists of size 4 tuples that represent a set of paths to draw
    start_color (string): Hexstring with hash code for the color of the start of the path
    end_color (string): Hexstring with hash code for the color of the end of the path
    """
    def create_file(self, filename='images/squares.png', 
                    path=[], max_path_len=0, paths=[], special=None, 
                    start_color='#FFFFFF40', end_color='#FF000040'):
        
        self.draw_tiling(self.generate_squares, filename, world=self.world, 
                         path=path, max_path_len=max_path_len, paths=paths, 
                         output_file=True, special=special, start_color=start_color, end_color=end_color)

    def get_raw_bytes(self, path=[], max_path_len=0, paths=[], 
                      special=None, start_color='#FFFFFF40', end_color='#FF000040', minor_path=None):
        
        return self.draw_tiling(self.generate_squares, filename='no.png', world=self.world, 
                                path=path, max_path_len=max_path_len, paths=paths, output_file=False, 
                                return_raw_bytes=True, special=special, start_color=start_color, end_color=end_color, minor_path=minor_path)

    def get_bytes(self, path=[], max_path_len=0, paths=[], special=None,
                  start_color='#FFFFFF40', end_color='#FF000040', minor_path=None):

        return self.draw_tiling(self.generate_squares, filename='no.png', world=self.world, path=path,
                                max_path_len=max_path_len, paths=paths, output_file=False, return_bytes=True, 
                                special=special, start_color=start_color, end_color=end_color, minor_path=minor_path)

    def create_file_and_get_bytes(self, filename='images/squares.png', 
                                  path=[], special=None, max_path_len=0, paths=[], start_color='#FFFFFF40', end_color='#FF000040'):

        return self.draw_tiling(self.generate_squares, filename, world=self.world, path=path, 
                                max_path_len=max_path_len, paths=paths, output_file=True, 
                                return_bytes=True, special=special)
