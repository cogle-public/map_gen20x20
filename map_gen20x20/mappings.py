from .codes import Codes, Colors

"""
    Color coding by level
    Level 0 : (121, 151, 0, 255),      # Height 0, light green
    Level 0 : (0, 34, 255, 255),       # Height 0, blue
    Level 0 : (95, 98, 57, 255),       # Height 0, green grey
    Level 1 : (0, 100, 14, 255),       # Height 1, dark green
    Level 2 : (160, 160, 160, 255),    # Height 2, grey
    Level 3 : (90, 90, 90, 255),       # Height 3, dark grey
    Level 4 : (0, 0, 0, 255),          # Height 4, black 
"""

COLOR_DICT = {
    Codes.TREES: (121, 151, 0, 255),     # Height 1, trees - light green
    Codes.GRASS: (121, 151, 0, 255),     # Height 0, grass - light green
    Codes.ROAD: (95, 98, 57, 255),     # Height 0, road - green grey
    Codes.AIRTRAFFIC_TOWER: (121, 151, 0, 255),  # Height 2, tower - light green (firewatch tower and air traffic tower)
    Codes.WATER: (0, 34, 255, 255),     # Height 0, water - blue
    Codes.FIREWATCH_TOWER: (121, 151, 0, 255),  # Height 2, tower - light green (firewatch tower and air traffic tower)
    Codes.HILL: (200, 200, 200, 255),   # Height 1, hills - light grey
    Codes.HIGH_HILL: (160, 160, 160, 255),   # Height 2, hills - grey
    Codes.FOOTHILL: (90, 90, 90, 255),      # Height 3, foothill - dark grey
    Codes.MOUNTAIN: (0, 0, 0, 255),         # Height 4, high mountains - black

    # Colors
    Colors.WHITE: (255, 255, 255, 200),
    Colors.BLACK: (0, 0, 0, 200),
    Colors.ORANGE: (255, 174, 0, 64),   # #FFAE00
    Colors.RED: (210, 0, 13, 64),      # #D2000D
    Colors.PINK: (255, 0, 204, 64),     # #FF00CC
    Colors.PURPLE: (156, 0, 255, 64),   # #9C00FF
    Colors.BLUE: (19, 87, 240, 64),      # #1357F0
    Colors.TEAL: (0, 241, 158, 64),    # #00F19E

    # Specials
    Codes.PACKAGE: (128, 0, 128, 255),  # Package Special image
    Codes.BROKEN_PACKAGE: (128, 0, 128, 255),    # Broken Package Special image
    Codes.HIKER: (255, 215, 0, 255),    # Hiker
    Codes.DROP_POINT: (255, 215, 0, 255),    # Drop Point
    Codes.DROP_POINT_BULLSEYE: (255, 215, 0, 255),    # Drop on Drop Point
    Codes.EXPLOSION: (255, 0, 0, 200), # Craft crash
    Codes.DROP_POINT_ARROW: (0, 0, 0, 255),
    Codes.START: (0, 0, 0, 255),
    Codes.STOP: (0, 0, 0, 255),
}

# When using iconography (mode==1), the icons are a mask (with masking color from color_dict) over a
# background color
BG_COLOR_DICT = {
    Codes.TREES: (0, 0, 0, 255),         # Black shows through mask
    Codes.GRASS: (0, 0, 0, 255),          # Black shows through mask
    Codes.ROAD: (0, 0, 0, 255),         # Black shows through mask
    Codes.AIRTRAFFIC_TOWER: (0, 0, 0, 255),         # Black shows through mask
    Codes.WATER: (255, 255, 255, 255),   # White shows through mask
    Codes.FIREWATCH_TOWER: (0, 0, 0, 255),         # Black shows through mask
    Codes.HILL: (0, 0, 0, 255),          # Black shows through mask
    Codes.HIGH_HILL: (0, 0, 0, 255),          # Black shows through mask
    Codes.FOOTHILL: (0, 0, 0, 255),          # Black shows through mask
    Codes.MOUNTAIN: (255, 255, 255, 255),    # White shows through mask
    
    # Specials
    Codes.PACKAGE: (0, 0, 0, 0),
    Codes.BROKEN_PACKAGE: (255, 215, 0, 164),    # Black shows through mask
    Codes.HIKER: (0, 0, 0, 0),          # No color, indicates a write it png
    Codes.DROP_POINT: (255, 215, 0, 164),    # Gold shows through mask
    Codes.DROP_POINT_BULLSEYE: (255, 215, 0, 164),
    Codes.EXPLOSION: (0, 0, 0, 0), # Craft crash
    Codes.DROP_POINT_ARROW: (0, 0, 0, 255),
    Codes.START: (0, 0, 0, 0),
    Codes.STOP: (0, 0, 0, 0)
}

# When using iconography, the icons are a mask (with masking color from color_dict) over a background color
IMG_PATH_DICT = {
    Codes.TREES:'images/trees-mask-small.png',      # Height 1, trees
    Codes.GRASS: None,                               # Height 0, grass, suppress icon 'images/grass-mask-small.png'
    Codes.ROAD: 'images/road-mask-small.png',      # Height 0, road
    Codes.AIRTRAFFIC_TOWER: 'images/tower-mask-small.png',     # Height 2, tower
    Codes.WATER: None,                              # Height 0, water - blue  # 'images/waves-mask-small.png'
    Codes.FIREWATCH_TOWER: 'images/tower-mask-small.png',     # Height 2, tower
    Codes.HILL: 'images/low-hill-mask-small.png',   # Height 1, low hill/high round
    Codes.HIGH_HILL: 'images/hill-mask-small.png',       # Height 2, high hill
    Codes.FOOTHILL: 'images/mtn-mask-small.png',        # Height 3, foothill
    Codes.MOUNTAIN: 'images/mtn-high-mask-small.png',        # Height 4, mountains
    
    # Specials
    Codes.PACKAGE: 'images/package.png',         # Package Special image
    Codes.BROKEN_PACKAGE: 'images/broken-package-mask-small.png',  # Broken Package Special image
    Codes.HIKER: 'images/hiker.png',                # Broken Package Special image
    Codes.DROP_POINT: 'images/drop-point-small.png',      # Drop point Special image
    Codes.DROP_POINT_BULLSEYE: 'images/drop-on-drop-point-mask-small.png',      # BDrop on Drop Point Special image
    Codes.EXPLOSION: 'images/explosion.png',
    Codes.DROP_POINT_ARROW: 'images/drop_point_arrow.png',
    Codes.START: 'images/start.png',
    Codes.STOP: 'images/finish.png',
}