# map_gen20x20

Map generator for use with 20x20 tile maps.  Can generate 2D top down visualizations using the MapGenerator object as well as 1D orthogonal views (altitude graph) using the AltGenerator object.

# Installation
Within your activated virtual environment, do `pip3 install git+https://gitlab.com/cogle-public/map_gen20x20`

# Usage
    from map_gen20x20 import MapGenerator, AltGenerator

    # tile_map (list): size (x, y) 2D list of integers that represents the map
    # mode (int): flag that accepts 0 or 1 - mode 0 generates a low fidelity image with no iconography.  Mode 1   #             augments colored tiles with icons.
    # width (int): width of result image in pixels
    # height (int): height of result image in pixels
    # side (int): length/width of a side
    # path (list): list of size 4 tuples (x, y, z, heading) that represents a path to draw. 
    # paths (list): 2D list - list of lists of size 4 tuples that represent a set of paths to draw
    # start_color (string): Hexstring with hash code for the color of the start of the path
    # end_color (string): Hexstring with hash code for the color of the end of the path
    # special (dict): special icons (hiker, drop point) to draw in addition to map, trace, and terrain icons. Key/Value pair is coordinates (x,y) / list of icon codes

    specials = {
        (11, 8): [Codes.DROP_POINT],
        (7, 8): [Codes.HIKER]
    }

    mg = MapGenerator(map=tile_map, width=400, height=400, side=20, path=[], mode=1)

    mg.create_file(filename='tests/results/squares.png', path=example_path, paths=[], special=specials, start_color='#FF000040', end_color='#FFFFFF40')

    ag = AltGenerator(map=tile_map, width=400, height=400, side=20, path=[], mode=1)

    ag.create_file(filename='tests/results/alt_squares.png', path=example_path, paths=[], special=specials, start_color='#FF000040', end_color='#FFFFFF40')

# Functions:

    # path (list): list of size 4 tuples (x, y, z, heading) that represents a path to draw. 
    # paths (list): 2D list - list of lists of size 4 tuples that represent a set of paths to draw
    # minor_path (list): 2d list - list of lists of size 4 tuples that represent a smaller path to draw
    # start_color (string): Hexstring with hash code for the color of the start of the path
    # end_color (string): Hexstring with hash code for the color of the end of the path
    # special (dict): special icons (hiker, drop point) to draw in addition to map, trace, and terrain icons. Key/Value pair is coordinates (x,y) / list of icon codes  
    # overlayVals (dict): dictionary of values with timestep/position/score information. 
    # overlayMonochrome (bool): Default is True.  Changes color of overlay to the monochrome color spefified in `penalty_config.json`.  Has no effect if overlayVals is not specified.  
    # schema (string): Default is "4".  Sets schema to use for overlay layout.

| function | description |  
|---|---|  
| create_file(filename, path, paths, special, start_color, end_color, overlayVals, overlayMonochrome, schema) | Generate an image and save to file |  
| get_bytes(filename, path, paths, special, start_color, end_color, minor_path, overlayVals, overlayMonochrome, schema) | Generate an image and output base64 format. |  
| create_file_and_get_bytes(filename, path, paths, special, start_color, end_color, overlayVals, overlayMonochrome, schema) | Generate an image, save to file and output base64 format. |  

# Icon Codes: 
 For ease of use, the Icon Codes are available as IntEnums when you import the "Codes" module  
 *: Special icons - use for multi colored elements.
| Code | Tile type | Tile Color (RGBA) |  
|---|---|---|  
| 1 | trees | (121, 151, 0, 255) |  
| 2 | grass | (121, 151, 0, 255) |   
| 10 | road | (95, 98, 57, 255) |  
| 13 | air traffic tower | (121, 151, 0, 255) |  
| 15 | river/lake | (0, 34, 255, 255) |  
| 17 | firewatch tower | (121, 151, 0, 255) |  
| 24 | low hill | (200, 200, 200, 255) |  
| 25 | hill | (160, 160, 160, 255) |  
| 26 | foothill | (90, 90, 90, 255) |  
| 31 | mountain | (0, 0, 0, 255) |  
| 100 | Package* | (128, 0, 128, 255) |  
| 101 | Broken Package* | (128, 0, 128, 255) |  
| 102 | Hiker* | (255, 215, 0, 255) |  
| 103 | Drop Point* | (255, 215, 0, 255) |  
| 104 | Drop on Drop Point* | (255, 215, 0, 255) |
| 105 | Explosion* | (255, 0, 0, 200) |  
| 106 | Drop point arrow* | (0, 0, 0, 255) |  


