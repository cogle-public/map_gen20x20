# Run this in the root directory!

activate () {
    source ./VENV/bin/activate
    pip3 install -r requirements.txt
}

# Make VENV
rm -rf VENV
virtualenv VENV
activate