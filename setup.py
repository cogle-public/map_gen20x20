from setuptools import setup, find_packages
 
setup(name='map_gen20x20',
      version='1.1.6',
      url='https://gitlab.com/cogle-public/map_gen20x20',
      author='Jacob Le, Michael Youngblood, Bob Krivacic',
      author_email='jacob.le@parc.com, youngbld@parc.com. krivacic@parc.com',
      description='Map generator for 20x20 tile maps.',
      packages=find_packages(exclude=['tests']),
      include_package_data=True,
      package_data={
            "": ["*.png", "*.json"],
      },
      long_description=open('README.md').read(),
      zip_safe=False)